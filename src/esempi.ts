const anno = 2020;

const esempioShortObjSintax = {
  prop1: 'myVal1',
  prop2: `ciao ragazz* benvenut*
alla accademy ${anno}`
};


let varStringEsempio = 'ciao ragazz* benvenut*\nalla accademy ' + anno;


const myFunction = (param1) => {
  console.log(param1 + 'ciao!');
};

interface IPippo {
  nome: string;
  cognome?: string;
}

class Utente implements IPippo {
  nome: string = 'default';
}

export class Pippo {
  public nome: string = 'cubo';
  public cognome: string = 'cubo';
}
