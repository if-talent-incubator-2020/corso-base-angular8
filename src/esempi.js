"use strict";
exports.__esModule = true;
exports.Pippo = void 0;
var anno = 2020;
var esempioShortObjSintax = {
    prop1: 'myVal1',
    prop2: "ciao ragazz* benvenut*\nalla accademy " + anno
};
var varStringEsempio = 'ciao ragazz* benvenut*\nalla accademy ' + anno;
var myFunction = function (param1) {
    console.log(param1 + 'ciao!');
};
var Utente = /** @class */ (function () {
    function Utente() {
        this.nome = 'default';
    }
    return Utente;
}());
var Pippo = /** @class */ (function () {
    function Pippo() {
        this.nome = 'cubo';
        this.cognome = 'cubo';
    }
    return Pippo;
}());
exports.Pippo = Pippo;
