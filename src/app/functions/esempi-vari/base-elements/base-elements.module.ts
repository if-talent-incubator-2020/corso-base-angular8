// import {NgModule} from '@angular/core';
// import {CommonModule} from '@angular/common';
// import {CardComponent} from './card.component';
// import {AppBgPrimaryDirective} from './app-bg-primary.directive';
// import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// import {EsempioComponentComponent} from './esempio-component/esempio-component.component';
// import {ALL} from 'tslint/lib/rules/completedDocsRule';
//
// const ALL_COMPONENTS = [CardComponent, AppBgPrimaryDirective];
//
// @NgModule({
//   declarations: [...ALL_COMPONENTS],
//   exports: [
//     ...ALL_COMPONENTS
//   ],
//   imports: [
//     CommonModule,
//     FormsModule,
//     ReactiveFormsModule
//   ]
// })
// export class BaseElementsModule {
// }
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DefaultPanelComponent} from './default-panel.component';
import {CardUtenteComponent} from './card-utente.component';
import {GreenBackgroundDirective} from './green-background.directive';
import {PippoPipe} from './pippo.pipe';
import {GestioneUtentiService} from './gestione-utenti.service';

const MODULE_COMPONENTS = [DefaultPanelComponent, CardUtenteComponent];
const MODULE_DIRECTIVES = [GreenBackgroundDirective];
const MODULE_PIPES = [PippoPipe];

@NgModule({
  imports: [CommonModule],
  declarations: [...MODULE_COMPONENTS, ...MODULE_DIRECTIVES, ...MODULE_PIPES],
  exports: [...MODULE_COMPONENTS, ...MODULE_DIRECTIVES, ...MODULE_PIPES],
  providers: [GestioneUtentiService]
})
export class BaseElementsModule{

}
