import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-default-panel',
  template: `
    <div class="card m-5" >
      <div class="card-header" *ngIf="pnlTitle">
        <span class="card-title" >{{pnlTitle}}</span >
      </div >
      <div class="card-body" >
        <ng-content ></ng-content >
      </div >
      <div class="card-footer" *ngIf="pnlFooter">{{pnlFooter}}</div >
    </div >
  `
})
export class DefaultPanelComponent {

  @Input()
  public pnlTitle: string;

  @Input()
  public pnlFooter: string;

  constructor() {
  }
}
