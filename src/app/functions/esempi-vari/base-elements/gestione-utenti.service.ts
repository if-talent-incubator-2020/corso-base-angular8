import {Injectable} from '@angular/core';
import {IUtenteModels} from '../../../models/IUtente.models';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs';

@Injectable({providedIn: 'root'})
export class GestioneUtentiService {


  listUtenti: BehaviorSubject<IUtenteModels[]> = new BehaviorSubject<IUtenteModels[]>([]);

  constructor(private http: HttpClient) {
  }

  public initArrayUtenti = () => {
    this.http.get<IUtenteModels[]>('http://localhost:3000/users', {}).subscribe((data: IUtenteModels[]) => {
      console.log(data);

      this.listUtenti.next(data);
      // console.log(this.listUtenti);
    });
    return [];
  };

  pushNewUtente = (ev) => {
    if (ev.stopPropagation) {
      ev.stopPropagation();
    }

    this.listUtenti.next([...this.listUtenti.getValue(), {username: 'utente' + new Date()}]);
  };

  removeLast = (ev) => {
    if (ev.stopPropagation) {
      ev.stopPropagation();
    }
    this.listUtenti.next(this.listUtenti.getValue().slice(0, this.listUtenti.getValue().length - 1));
  };
}
