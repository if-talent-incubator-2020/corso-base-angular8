import {Directive, ElementRef, OnInit} from '@angular/core';

@Directive({
  selector: '[appGreenBackground]'
})
export class GreenBackgroundDirective implements OnInit {

  constructor(private elRef: ElementRef) {

  }

  ngOnInit() {
    console.log(this.elRef.nativeElement);
    this.elRef.nativeElement.style = 'background-color: #00FF00';
  }

}
