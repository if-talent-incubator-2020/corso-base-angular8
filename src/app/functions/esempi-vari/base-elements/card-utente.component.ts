import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {IUtenteModels} from '../../../models/IUtente.models';

@Component({
  selector: 'app-card-utente',
  template: `
    <div class="card-body" *ngIf="utente" (click)="cardClickHandler($event)" >
      <div class="row" >
        <div class="form-group col-6" ><label >username:</label >{{utente.username}}</div >
        <div class="form-group col-6" ><label >nome:</label >{{utente.nome}}</div >
        <div class="form-group col-6" ><label >cognome:</label >{{utente.cognome}}</div >
        <div class="form-group col-6" ><label >ufficio:</label >{{utente.ufficio}}</div >
      </div >
    </div >
  `
})
export class CardUtenteComponent implements OnInit, OnChanges, OnDestroy {

  @Input()
  public utente: IUtenteModels;

  @Output()
  public selected: EventEmitter<IUtenteModels> = new EventEmitter<IUtenteModels>();

  constructor() {
    // this.selected = new EventEmitter<IUtenteModels>();
  }

  cardClickHandler = (ev) => {
    if (ev.stopPropagation) {
      ev.stopPropagation();
    }
    this.selected.emit(this.utente);
  };

  ngOnInit() {
    // this.selected = new EventEmitter<IUtenteModels>(); ---> SBAGLIATO!
    console.log('init', this.utente);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('changed', changes);
  }

  ngOnDestroy() {
    console.log('destroy', this.utente);
  }
}
