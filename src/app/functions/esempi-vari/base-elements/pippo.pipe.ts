import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'pippo'
})
export class PippoPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    console.log(value);
    return this.concat(value.length);
  }


  concat = (n): string => {
    let out = '';
    for (let i = 0; i < n; i++) {
      out += 'pippo ';
    }
    return out;
  };


}
