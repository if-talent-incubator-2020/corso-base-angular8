import {Directive, ElementRef, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appBgPrimary]'
})
export class AppBgPrimaryDirective implements OnInit {

  constructor(private renderer: Renderer2, private el: ElementRef) {
    this.renderer.addClass(this.el.nativeElement, 'bg-primary');
  }

  ngOnInit(): void {

  }


}
