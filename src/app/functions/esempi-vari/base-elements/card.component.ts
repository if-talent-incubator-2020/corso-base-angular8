import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-card',
  template: `
    <div class="card" [ngClass]="panelClass" >
      <div class="card-header" ng-if="panelTitle" appBgPrimary >{{panelTitle}}</div >
      <div class="card-body" >
        <ng-content ></ng-content >
<!--        <form [formGroup]="myForm">-->
<!--          <input type="text" formControlName="isbn" />-->
<!--          <input type="text" formControlName="titolo" />-->
<!--          <input type="text" formControlName="autore" />-->
<!--        </form>-->
      </div >
    </div >`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardComponent {

  @Input()
  panelClass: string[] | string;

  @Input()
  panelTitle: string;

  // myForm: FormGroup = null;

  constructor(private fb: FormBuilder) {
    // this.myForm = this.fb.group({
    //   isbn: this.fb.control({}, [], []),
    //   titolo: this.fb.control({}, [], []),
    //   autore: this.fb.control({}, [], [])
    // });
  }
}
