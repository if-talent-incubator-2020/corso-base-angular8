import {BehaviorSubject} from 'rxjs';

export class EsempiObservable {
  mioEsempioObs: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  mioEsempioObs2: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  mioEsempioObs3: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  mioEsempioObs4: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  mioEsempioObs5: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  mioEsempioObs6: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor() {
    this.mioEsempioObs.subscribe(this.myHandlerOk);
    this.mioEsempioObs.subscribe((value) => this.myHandlerOk(value));
    this.mioEsempioObs.subscribe(this.myHandlerOkRet());
  }

  public addValueToEsempio = (value: any) => {
    this.mioEsempioObs.next(value);

    let a = 10;
    const a = 10;
    var a = 10;
    a = 20;
  };

  myHandlerOk = (value?: any) => {
    return 10;
  };

  myHandlerOkRet = () => {
    return this.myHandlerOk;
  };

}
