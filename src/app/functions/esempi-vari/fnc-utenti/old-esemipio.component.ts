import {IUtenteModels} from '../../../models/IUtente.models';
import {Subscription} from 'rxjs';
import {GestioneUtentiService} from '../base-elements/gestione-utenti.service';
import {Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-old-component',
  template: `

    <app-default-panel [pnlTitle]="title" >questo è il mio nuovo contenuto del pannellino</app-default-panel >
    <app-default-panel >{{'questo pannello non ha titolo' | pippo}}</app-default-panel >
    <app-default-panel >{{oggi | date}} - {{oggi}}</app-default-panel >
    <button (click)="title = 'corso-base-angular8'" >titolo 1</button >
    <button (click)="title = 'corso di angular8+ ed 2021'" >titolo 2</button >

    <button (click)="pushNewUtente($event)" appGreenBackground >add user</button >
    <button (click)="removeLast($event)" >remove last</button >

    <app-default-panel [pnlTitle]="'elenco utenti'" >
      <div class="card" >
        <app-card-utente [utente]="selectedUtente" ></app-card-utente >
      </div >
      <app-card-utente *ngFor="let user of listUtenti" [utente]="user" (selected)="getSelectedUserHandler($event)" ></app-card-utente >
    </app-default-panel >

  `,
  styles: []
})
export class OldEsemipioComponent implements OnInit, OnDestroy {
  title = 'corso-base-angular8';
  public oggi = new Date();

  selectedUtente: IUtenteModels = null;

  public listUtenti;

  _listUtenti$: Subscription;

  ngOnInit() {
    this._listUtenti$ = this.svc.listUtenti.subscribe((v) => this.listUtenti = [...v]);
  }

  ngOnDestroy() {
    if (this._listUtenti$) {
      this._listUtenti$.unsubscribe();
    }
  }

  //private svc: GestioneUtentiService; --> ometto se metto public/private sul costruttore

  constructor(private svc: GestioneUtentiService) {
    this.svc.initArrayUtenti();

  }

  pushNewUtente = (ev) => {
    this.svc.pushNewUtente(ev);
  };

  removeLast = (ev) => {
    this.svc.removeLast(ev);
  };


  getSelectedUserHandler = (ev) => {
    this.selectedUtente = ev;
  };
}
