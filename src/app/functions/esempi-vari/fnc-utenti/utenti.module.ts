import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OldEsemipioComponent} from './old-esemipio.component';
import {BaseElementsModule} from '../base-elements/base-elements.module';
import {UtentiContainer} from './utenti.container';
import {CardComponent} from '../base-elements/card.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

const MODULE_COMPONENTS = [OldEsemipioComponent, UtentiContainer];

@NgModule({
  imports: [CommonModule, BaseElementsModule, FormsModule, ReactiveFormsModule],
  declarations: [...MODULE_COMPONENTS, CardComponent],
  exports: [...MODULE_COMPONENTS]
})
export class UtentiModule {

}
