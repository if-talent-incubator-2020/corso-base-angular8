import {Injectable} from '@angular/core';
import {UtentiState} from './utenti.state';
import {HttpClient} from '@angular/common/http';
import {IUtenteModels} from '../../../models/IUtente.models';
import {environment} from '../../../../environments/environment';

@Injectable({providedIn: 'root'})
export class UtentiService {

  readonly fncUrl: string = `${environment.baseUrl}/users`;

  constructor(private state: UtentiState, private http: HttpClient) {
  }

  search = (qryParam?: any): void => {
    console.log('search', qryParam);
    this.http.get<IUtenteModels[]>(this.fncUrl, {params: {...qryParam}}).subscribe((data) => {

      setTimeout(() => {
        this.state.listUsers.next([...data]);
      }, 1000);

    });
  };


  utenteSelected = (ev: any, user: IUtenteModels) => {
    if (ev.stopPropagation) {
      ev.stopPropagation();
    }

    setTimeout(() => {
      this.state.userSelected.next(user);
    }, 1000);
  };


  funzioneParadigma3 = () => {
    this.funzioneParadigmaFunzionale2(this.funzioneParadigmaFuzionale({username: 'aaa'}));
  };

  funzioneParadigmaFunzionale2 = (valore: IUtenteModels) => {
    if (valore) {
      let out: IUtenteModels = {...valore};
      if (!out.cognome) {
        out.cognome = 'non valorizzato';
      }
      return out;
    } else {
      return null;
    }

  };

  funzionePura = () => {
    return 1 + 1;
  };

  funzioneNonPura = (valore) => {
    // return chiamaBackend(valore)
  };


  funzioneParadigmaFuzionale = (valore: IUtenteModels) => {
    valore.username = 'altro';
    return valore;
  };
}
