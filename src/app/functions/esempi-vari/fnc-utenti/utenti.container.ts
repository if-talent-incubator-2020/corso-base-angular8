import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {UtentiService} from './utenti.service';
import {UtentiState} from './utenti.state';

@Component({
  selector: 'app-utenti-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <app-card >
      <form (ngSubmit)="search()" >
        <div class="row" >
          <div class="form-group" >
            <label >id</label >
            <input type="text" class="form-control" [ngModel]="id" (ngModelEvent)="id = $event" [ngModelOptions]="{standalone:true}" /></div >
          <button type="submit" class="btn btn-primary" >search</button >
        </div >
      </form >
    </app-card >
    <!--    <app-card-utente [utente]="state.userSelected | async" ></app-card-utente >-->
    <table class="table" >
      <thead >
      <tr >
        <th >_id</th >
        <th >username</th >
      </tr >
      </thead >
      <tbody >
      <tr *ngFor="let user of (state.listUsers | async)" >
        <td >{{user._id}}</td >
        <td >{{user.username}}</td >
        <td >
          <button class="btn btn-primary" (click)="tblRowSelect($event, user)" >select</button >
        </td >
      </tr >
      </tbody >
    </table >
    {{showContentUserSelected() | json}}
    {{state.userSelected | async | json}}
  `
})
export class UtentiContainer implements OnInit, OnDestroy {

  id: number;

  constructor(private service: UtentiService, public state: UtentiState) {

  }

  tblRowSelect = (ev, user) => {
    this.service.utenteSelected(ev, user);
  };

  showContentUserSelected = () => {
    return this.state.userSelected.getValue();
  };

  search = () => {
    this.service.search({_id: this.id});
  };

  ngOnDestroy(): void {

  }

  ngOnInit(): void {
    this.service.search();
    setTimeout(() => {
      console.log(this.state.listUsers);
    }, 500);
  }

}
