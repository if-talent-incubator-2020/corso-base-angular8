import {Injectable} from '@angular/core';
import {IUtenteModels} from '../../../models/IUtente.models';
import {BehaviorSubject} from 'rxjs';

@Injectable({providedIn: 'root'})
export class UtentiState {
  public userSelected: BehaviorSubject<IUtenteModels>;
  public listUsers: BehaviorSubject<IUtenteModels[]>;

  constructor() {
    this.userSelected = new BehaviorSubject<IUtenteModels>(null);
    this.listUsers = new BehaviorSubject<IUtenteModels[]>([]);
  }
}
