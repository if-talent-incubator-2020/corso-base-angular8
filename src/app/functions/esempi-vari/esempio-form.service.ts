import {AbstractControl, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {EsempioFormState} from './esempio-form.state';
import {Injectable} from '@angular/core';
import {IFormStructureModel} from '../../models/IFormStructure.model';
import {environment} from '../../../environments/environment';
import {IUtenteModels} from '../../models/IUtente.models';

@Injectable({
  providedIn: 'root'
})
export class EsempioFormService {

  readonly esempioFormUrl = `${environment.baseUrl}/esempio-form-utente`;
  readonly utentiUrl = `${environment.baseUrl}/users`;

  constructor(private fb: FormBuilder, private http: HttpClient, private state: EsempioFormState) {

  }

  getAllUsers = () => {
    this.http.get<IUtenteModels[]>(this.utentiUrl, {}).subscribe((data: IUtenteModels[]) => {
      this.state.users.next(data);
    });
  };

  initForm = (): FormGroup => {
    return this._initForm(this.state.formStructure.getValue());
  };

  initFormFromBackend = () => {
    this.http.get<IFormStructureModel[]>(this.esempioFormUrl, {}).subscribe((data: IFormStructureModel[]) => {
      this.state.formStructure.next(data);
    });
  };

  private _initForm = (formStructure: IFormStructureModel[]): FormGroup => {
    const ret = this.fb.group({});

    if (!!formStructure) {
      for (const control of formStructure) {
        const fc: FormControl = this.fb.control(null);
        // if (control.disabled) {
        //   fc.disable();
        // }

        ret.addControl(control.controlName, fc);
      }
    }

    return ret;
  };

  insertOrUpdateUser = (value: IUtenteModels) => {
    if (!!value.id) {
      this.updateUser(value);
    } else {
      this.addUser(value);
    }
  };

  addUser = (value: IUtenteModels) => {
    this.http.post<IUtenteModels>(this.utentiUrl, {...value}, {}).subscribe((data) => {
      console.log('salvataggio effettuato con successo', data);
      this.getAllUsers();
    });
  };

  updateUser = (value: IUtenteModels) => {
    this.http.put<IUtenteModels>(`${this.utentiUrl}/${value.id}`, {...value}, {}).subscribe((data) => {
      console.log('salvataggio effettuato con successo', data);
      this.getAllUsers();
    });
  };

  selectUtente = (myForm: FormGroup, utente: IUtenteModels) => {

    for (const control in utente) {
      const fc: AbstractControl = myForm.controls[control];
      fc.setValue(utente[control]);
    }
  };
}
