import {Component} from '@angular/core';

@Component({
  selector: 'app-esempi-base',
  template: `
    <div class="p-5" >
      <!--    <app-card panelTitle="Prova card" panelClass="bg-primary" >lorem ipsum</app-card >-->
      <div *ngIf="ifCase" >mio contenuto da nascondere</div >
      <p >{{myCurrencyVal | currency}}</p >
      <p >
        {{myArray}}<br />{{myArray | json}}
      </p >
      <div *ngFor="let elem of myArray" >mio contenuto da ripetere {{elem?.prop}}</div >
      <div [ngSwitch]="mySwitch" >
        <div *ngSwitchCase="'A'" >switch A</div >
        <div *ngSwitchCase="'B'" >switch B</div >
        <div *ngSwitchCase="'C'" >switch C</div >
        <div *ngSwitchDefault >ho sbagliato qualche cosa</div >
      </div >
    </div >

    <div class="main-body" >
      main-body
      <div class="left-bar" >left-bar</div >
      <div class="main-content" >main-content
        <h1 class="main-title" >main-title</h1 >
      </div >

    </div >
  `
})
export class EsempiBaseComponent {

  public myCurrencyVal = 1234.88;

  public ifCase = false;
  public myArray = [1, 2, 3, 4, null, {prop1: 'A'}];
  public mySwitch = 'C';
}
