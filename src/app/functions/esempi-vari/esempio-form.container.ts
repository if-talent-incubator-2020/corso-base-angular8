import {Component, ViewChild} from '@angular/core';
import {NgModel} from '@angular/forms';
import {IUtenteModels} from '../../models/IUtente.models';


@Component({
  selector: 'app-root',
  template: `
    <form class="p-5" #myForm="ngForm" >
      <input class="form-control col-6" type="text" [(ngModel)]="utente.username" required="true" name="utenteFld" #utenteFld="ngModel"
             #utenteFldRef />
      <input class="form-control col-6" type="text" [(ngModel)]="utente.nome" required="true" name="utenteFld2"
      />
      <input class="form-control col-6" type="text" [(ngModel)]="utente.cognome" required="true" name="utenteFld3"
      />
      <input class="form-control col-6" type="text" [(ngModel)]="utente.ufficio" required="true" name="utenteFld4"
      />
      <span class="bg-danger" *ngIf="utenteFld.invalid" >ERRORE!!!!</span >
      <span class="bg-success" *ngIf="utenteFld.valid" >V</span >
      {{utente | json}}{{utenteFldCode?.value}}{{utenteFld?.value}}
      <br />
      {{myForm.valid ? 'form valido' : 'form invalido'}}
      {{myForm.value | json}}
      <button class="btn btn-primary" (click)="doSomething(myForm.value)" [disabled]="myForm.invalid" >cerca</button >
    </form >
    <router-outlet ></router-outlet >
  `,
  styles: []
})
export class EsempioFormContainer {

  utente: IUtenteModels = {username: 'prova'};
  field: NgModel;

  @ViewChild('#utenteFldRef')
  utenteFldCode: NgModel;

  doSomething(ev) {
    console.log(ev);
    // this.utente = ev;

  }
}
