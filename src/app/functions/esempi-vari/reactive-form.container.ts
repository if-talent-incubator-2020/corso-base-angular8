import {Component} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {EsempioFormState} from './esempio-form.state';
import {EsempioFormService} from './esempio-form.service';
import {IUtenteModels} from '../../models/IUtente.models';

@Component({
  selector: 'app-reactive-form-controller',
  template: `
    <div >
      <input type="number" [(ngModel)]="fontSizePx" [ngModelOptions]="{standalone:true}" >
      <h1 style="font-size: {{fontSizePx}}px;" >mio titolo che si allarga e restringe</h1 >
    </div >
    <div class="card m-5" >
      <div class="card-body" >
        <form [formGroup]="myForm" class="form-row" *ngIf="(state.formStructure | async) && myForm" (submit)="aggiungiUtente($event)" >
          <div class="form-group" *ngFor="let control of (state.formStructure | async)" >
            <label >{{control.label}} ({{myForm?.controls[control.controlName]?.invalid}})</label >
            <input class="form-control" type="text" [formControlName]="control.controlName" [readOnly]="control.disabled" >
            <pre *ngIf="myForm?.controls[control.controlName]?.invalid" >{{myForm?.controls[control.controlName]?.errors | json}}</pre >
          </div >
          <button class="btn btn-primary btn-block" type="submit" [disabled]="myForm.invalid" >Aggiungi</button >
        </form >
      </div >
      <div class="card-footer" >
        <pre >invalid: {{myForm.invalid}}, touched: {{myForm.touched}}</pre >
        <pre >{{myForm.value | json}}</pre >
      </div >
    </div >
    <div class="card m-5" >
      <div class="card-body row" >
        <button class="btn btn-primary btn-block" type="button" (click)="recuperaUtenti($event)" >Recupera tutti gli utenti</button >
        <table class="table col-12" >
          <thead >
          <tr >
            <th *ngFor="let control of (state.formStructure | async)" >{{control.label}}</th >
          </tr >
          </thead >
          <tbody >
          <tr *ngFor="let utente of (state.users | async)" >
            <td *ngFor="let control of (state.formStructure | async)" >{{utente[control.controlName]}}</td >
            <td >
              <button type="button" class="btn btn-primary" (click)="selectUtente(utente)" >seleziona</button >
            </td >
          </tr >
          </tbody >
        </table >
      </div >
    </div >
    <router-outlet ></router-outlet >
  `,
  styles: []
})
export class ReactiveFormContainer {
  fontSizePx: number = 12;

  myForm: FormGroup;

  myControls: string[] = ['username', 'nome', 'cognome', 'ufficio', 'data_nascita'];

  _formStructure$: Subscription;

  constructor(private fb: FormBuilder, public state: EsempioFormState, private service: EsempioFormService) {
    //this.buildFormStatic();
    // this.buildFormDynamic();
    // this.myForm = this.service.initForm();
    this.service.initFormFromBackend();
    // this.service.getAllUsers();
  }

  ngOnInit() {
    this._formStructure$ = this.state.formStructure.subscribe((data) => this.myForm = this.service.initForm());
  }

  ngOnDestroy() {
    if (this._formStructure$) {
      this._formStructure$.unsubscribe();
    }
  }

  selectUtente = (utente: IUtenteModels) => {
    this.service.selectUtente(this.myForm, utente);
  };

  aggiungiUtente = (ev) => {
    if (ev.stopPropagation) {
      ev.stopPropagation();
    }

    this.service.insertOrUpdateUser(this.myForm.value);
  };

  recuperaUtenti = (ev) => {
    if (ev.stopPropagation) {
      ev.stopPropagation();
    }

    this.service.getAllUsers();
  };

  buildFormStatic = () => {
    this.myForm = this.fb.group({
      username: this.fb.control(null),
      nome: this.fb.control(null),
      cognome: this.fb.control(null),
      ufficio: this.fb.control(null),
    });
  };

  buildFormDynamic = () => {

    const validators: ValidatorFn[] = [Validators.minLength(2), Validators.minLength(3), Validators.maxLength(20), Validators.required];

    this.myForm = this.fb.group({});
    if (!!this.myControls) {
      for (const myFormKey of this.myControls) {

        this.myForm.addControl(myFormKey, this.fb.control(null, [...validators]));
      }
      console.log(this.myControls, this.myForm);
    }

  };

  corsoValidator(control: AbstractControl): { corsoValidator: boolean } {
    const ret = control.value === 'CORSO' ? null : {corsoValidator: true};
    console.log(control.value, ret, control.value === 'CORSO');
    return ret;
  }
}
