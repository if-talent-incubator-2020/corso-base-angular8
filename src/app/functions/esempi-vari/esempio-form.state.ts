import {BehaviorSubject} from 'rxjs';
import {IFormStructureModel} from '../../models/IFormStructure.model';
import {Injectable} from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class EsempioFormState {
  formStructure: BehaviorSubject<IFormStructureModel[]> = new BehaviorSubject<IFormStructureModel[]>([]);

  users: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
}
