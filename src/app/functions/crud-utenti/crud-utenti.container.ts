import {Component} from '@angular/core';

@Component({
  selector: 'app-crud-utenti-container',
  template: `
    <div class="card" >
      <div class="card-title" >qui siamo nella sezione che gestisce gli utenti</div >
      <li class="navbar-nav" ><a href="#" routerLink="details/1" routerLinkActive="active" >UTENTI</a >
      <div class="card card-body" >
        <router-outlet ></router-outlet >
      </div >
    </div >
  `
})
export class CrudUtentiContainer {

}
