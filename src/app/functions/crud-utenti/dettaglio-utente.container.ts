import {Component} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-dettaglio-utenti-container',
  template: `qui saremo nel dettaglio {{id}}-{{nome}}`
})
export class DettaglioUtenteContainer {
  id: string = null;
  nome: string = null;

  _paramMap$: Subscription;
  _queryParamMap$: Subscription;

  constructor(private activatedRoute: ActivatedRoute) {
    this._paramMap$ = this.activatedRoute.paramMap.subscribe((data: ParamMap) => {
      if (data.has('id')) {
        console.log('ho intercettato ID');
        this.id = data.get('id');
      }
    });
    this._queryParamMap$ = this.activatedRoute.queryParamMap.subscribe((data: ParamMap) => {
      if (data.has('nome')) {
        console.log('ho intercettato NOME');
        this.nome = data.get('nome');
      }
    });
  }
}
