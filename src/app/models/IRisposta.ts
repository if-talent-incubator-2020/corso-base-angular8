export interface IRisposta<T> {
  returnState: 'OK' | 'KO';
  returnMsg?: string;
  data?: T;
}
