export interface IUtenteModels {
  id?: number;
  username: string;
  nome?: string;
  cognome?: string;
  dataNascita?: string;
  ufficio?: string;
};
