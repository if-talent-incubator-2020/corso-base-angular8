export interface IFormStructureModel {
  controlName: string;
  label?: string;
  controlType?: 'TEXT' | string;
  validatorNames?: string[];
  formGroupClass?: string[];
  formControlClass?: string[];
  disabled?: boolean;
}
