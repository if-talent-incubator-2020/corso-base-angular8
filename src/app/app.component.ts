import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div class="card-body row" >
      <div class="card col-4" >
        <nav class="navbar navbar-dark" >
          <ul >
            <li class="navbar-nav" ><a href="#" routerLink="/home" routerLinkActive="active" >Home</a ></li >
            <li class="navbar-nav" ><a href="#" routerLink="/reactive-form-example" routerLinkActive="active" >esempio reactive form</a >
            <li class="navbar-nav" ><a href="#" routerLink="/template-driven-form-example" routerLinkActive="active" >esempio template driven form</a >
            <li class="navbar-nav" ><a href="#" routerLink="/utenti" routerLinkActive="active" >UTENTI</a >
            <li class="navbar-nav" ><a href="#" routerLink="/reactive-form-example" routerLinkActive="active" >esempio reactive form</a >
            <li class="navbar-nav" ><a href="#" routerLink="/reactive-form-example" routerLinkActive="active" >esempio reactive form</a >
            </li >
          </ul >
        </nav >
      </div >
      <div class="card col-8" >
        <router-outlet ></router-outlet >
      </div >
    </div >
  `,
  styles: []
})
export class AppComponent {


}
