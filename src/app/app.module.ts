import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BaseElementsModule} from './functions/esempi-vari/base-elements/base-elements.module';
import {EsempiBaseComponent} from './functions/esempi-vari/esempi-base.component';
import {HttpClientModule} from '@angular/common/http';
import {UtentiModule} from './functions/esempi-vari/fnc-utenti/utenti.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EsempioFormContainer} from './functions/esempi-vari/esempio-form.container';
import {ReactiveFormContainer} from './functions/esempi-vari/reactive-form.container';

@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BaseElementsModule,
    HttpClientModule,
    UtentiModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  // constructor(private customProv: MyService) {
  //
  //
  // }

}
