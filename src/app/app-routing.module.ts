import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ReactiveFormContainer} from './functions/esempi-vari/reactive-form.container';
import {EsempiBaseComponent} from './functions/esempi-vari/esempi-base.component';
import {EsempioFormContainer} from './functions/esempi-vari/esempio-form.container';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HomeController} from './functions/home.controller';
import {UtentiContainer} from './functions/esempi-vari/fnc-utenti/utenti.container';
import {UtentiModule} from './functions/esempi-vari/fnc-utenti/utenti.module';
import {CrudUtentiContainer} from './functions/crud-utenti/crud-utenti.container';
import {DettaglioUtenteContainer} from './functions/crud-utenti/dettaglio-utente.container';
import {RicercaUtenteContainer} from './functions/crud-utenti/ricerca-utente.container';

const routes: Routes = [
  {path: 'home', component: HomeController},
  {path: 'reactive-form-example', component: ReactiveFormContainer},
  {path: 'template-driven-form-example', component: EsempioFormContainer},
  {path: 'old-esempio-utente', component: UtentiContainer},
  {
    path: 'utenti', component: CrudUtentiContainer, children: [
      {path: 'ricerca', component: RicercaUtenteContainer},
      {path: 'details/:id', component: DettaglioUtenteContainer},
      {path: '', redirectTo: 'ricerca', pathMatch: 'full'}
    ]
  },
  {path: '', redirectTo: 'home', pathMatch: 'full'}
];
const MODULE_CONTAINERS = [EsempiBaseComponent,
  EsempioFormContainer,
  HomeController,
  ReactiveFormContainer,
  CrudUtentiContainer,
  DettaglioUtenteContainer,
  RicercaUtenteContainer

];

@NgModule({
  declarations: [...MODULE_CONTAINERS],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, RouterModule.forRoot(routes, {useHash: true}), UtentiModule],
  exports: [RouterModule, ...MODULE_CONTAINERS]
})
export class AppRoutingModule {
}
